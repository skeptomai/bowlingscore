import React from 'react';
import './App.css';
import Menu from './Component/Menu/Menu';

function App() {
  return (
    <div className="App">
      <Menu />
    </div>
  );
}

export default App;
