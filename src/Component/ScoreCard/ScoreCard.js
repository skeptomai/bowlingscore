import React, {Component} from 'react';
import './ScoreCard.css';
import posed from 'react-pose';
import p1img from '../../img/p1.png';
import p2img from '../../img/p2.png';

const Box = posed.div({
    visible: {
        outlineWidth: '0px',
        outlineOffset: '0px',
        scale: 1
    },
    winner: {
        outlineWidth: '12px',
        outlineOffset: '5px',
        outlineColor: '#ffe10e',
        scale: 1.2
    }
  });

  class ScoreCard extends Component {


    render() {
        return (
        <Box className="card" pose={this.props.pose}>
            <table>
                <tbody>
                    <tr>
                        <td className="playerInfo" key="pimg"><img src={(this.props.player === 'player1' || this.props.player === 'player') ? p1img : p2img} alt={this.props.player}/></td>

                        {[1,2,3,4,5,6,7,8,9,10].map(frame =>{
                        return <td key={frame}> {this.props.score[frame][0]} {this.props.score[frame][1]} {(frame === 10) ? this.props.score[frame][2] : '' }</td>
                        })}
                        <td className="total" key="tot"><span>total:</span><span>{this.props.total}</span></td>
                    </tr>
                </tbody>
            </table>
            
            
        </Box>
            )
        }

}

export default ScoreCard;