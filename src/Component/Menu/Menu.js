import React, { Component } from 'react';
import './Menu.css';
import {ModeButton, ScoresButton} from '../Buttons/Buttons';
import Game from '../Game/Game';
import posed, { PoseGroup } from 'react-pose';
import {Modal, Shade} from '../Modal/Modal';

const Prevscore = posed.li({
    hoverable: true,
    init: {
        borderStyle: 'solid',
        borderWidth: 0,
        borderColor: 'white'
    },
    hover: {
        borderStyle: 'solid',
        borderWidth: 3,
        borderColor: 'white'
    },
})

class Menu extends Component {
    constructor(props){
        super(props)
        this.state = {
            selectedMode: -1,
            showScoresModal: false,
            loadedScore: ''
        }
    }

    componentDidMount(){
        this.setState({selectedMode: 0})
    }

    //Activates One Player Mode
    onePlayerMode() {
        if (this.state.selectedMode > 0) return;
        this.setState({selectedMode: 1});
    }

    //Activates Two Players Mode
    twoPlayerMode() {
        if (this.state.selectedMode > 0) return;
        this.setState({selectedMode: 2});
    }

    // Opens the modal to select and view previous scores
    showScores() {
        if (this.state.selectedMode > 0) return;
        this.setState({showScoresModal: !this.state.showScoresModal});
    }

    // Loads a previous score
    loadScore(evt){
        var score =  JSON.parse(localStorage.getItem(evt.target.getAttribute('ts')))
        this.setState({
            selectedMode: score.state.playersNumber,
            loadedScore: score,
            showScoresModal: false
        })
    }

    render(){
        return (
        <div>
            { (this.state.selectedMode ===0) ?
            <div className="modeSelector">
                <div onClick={this.onePlayerMode.bind(this)}><ModeButton number="1" pose={this.state.selectedMode===0 ? 'init' : 'hidden'}></ModeButton></div>
                <div onClick={this.twoPlayerMode.bind(this)}><ModeButton number="2" pose={this.state.selectedMode===0 ? 'init' : 'hidden'}></ModeButton></div>
            </div> : ''}
            { (this.state.selectedMode ===0) ? <div onClick={this.showScores.bind(this)}><ScoresButton  pose={this.state.selectedMode===0 ? 'init' : 'hidden'}></ScoresButton></div> : ''}

            
            { (this.state.selectedMode >0) ? <Game mode={this.state.selectedMode} loadedScore={this.state.loadedScore} /> : ''}
            

            <PoseGroup>
                {this.state.showScoresModal && [
                <Shade key="shade" className="shade" />,
                <Modal key="modal" className="modal" >
                    <ul>
                        {Object.entries(localStorage).map(game => {return <Prevscore key={game[0]}><div onClick={this.loadScore.bind(this)} ts={game[0]}>{game[0]}</div></Prevscore> }) }
                        <Prevscore key='close' className="closeButton"><div onClick={this.showScores.bind(this)}>Close</div></Prevscore>
                    </ul>
                </Modal>
                ]}
            </PoseGroup>
        
        </div>

            
        );
    }

}
export default Menu;