import posed from 'react-pose';


const Modal = posed.div({
    enter: {
      y: 0,
      opacity: 1,
      delay: 300,
      transition: {
        y: { type: 'spring', stiffness: 1000, damping: 15 },
        default: { duration: 300 }
      }
    },
    exit: {
      y: 50,
      opacity: 0,
      transition: { duration: 150 }
    }
  });

const Shade = posed.div({
    enter: { opacity: 1 },
    exit: { opacity: 0 }
});

const Animation = posed.div({
  enter: {
    opacity: 1
  },
  exit: {
    opacity: 0,
    transition: {duration: 10}
  }
});


export {Modal, Shade, Animation}