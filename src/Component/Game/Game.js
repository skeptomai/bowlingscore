import React, { Component } from 'react';
import './Game.css';
import ScoreCard from '../ScoreCard/ScoreCard';
import posed, {PoseGroup} from 'react-pose';
import {Animation, Shade} from '../Modal/Modal';
import Strike from '../../img/strike.gif';

const Keyboard = posed.div({
    visible: { opacity: 1, delay: 300  },
    hidden: { opacity: 0 }
  });

const Cards = posed.div({
    visible: { opacity: 1, delay: 300  },
    hidden: { opacity: 0 }
  });


class Game extends Component {
    constructor(props){
        super(props);
        if (this.props.loadedScore === undefined || this.props.loadedScore ===''){
            this.state = {
                playersNumber: this.props.mode,
                players: [],
                progress: {
                    player: '',
                    shots: 0,
                    frame: 0
                },
                endgame: false,
                animation: false
            }
            this.frameScores = {}
            this.scores = {}
    }
        else {
            this.state = this.props.loadedScore.state;
            this.scores = this.props.loadedScore.scores;
            this.frameScores = this.props.loadedScore.frameScores;
        }
        this.onScoreInput = this.onScoreInput.bind(this);
    }

    // Both the contstructor check if the game is a 1/2 players game or a loaded score
    componentDidMount(){
        if (this.state.playersNumber === 1 && this.state.progress.frame === 0){
            this.setState({
                playersNumber: 1,
                players: ['player'],
                progress:{
                    player: 'player',
                    shots: 0,
                    frame: 1
                    }
                });
            this.scores= {
                player:[['',''],['',''],['',''],['',''],['',''],['',''],['',''],['',''],['',''],['',''],['','',''],['',''],[''],['']]
            }
            this.frameScores= {
                player:[]
            }
        }

        else if (this.state.playersNumber === 2 && this.state.progress.frame === 0){
            this.setState({playersNumber: 2,
                players: ['player1', 'player2'],
                progress:{
                    player: 'player1',
                    shots: 0,
                    frame: 1
                }

            });
            this.scores= {
                player1:[['',''],['',''],['',''],['',''],['',''],['',''],['',''],['',''],['',''],['',''],['','',''],['',''],[''],['']],
                player2:[['',''],['',''],['',''],['',''],['',''],['',''],['',''],['',''],['',''],['',''],['','',''],['',''],[''],['']]
            }
            this.frameScores= {
                player1:[],
                player2:[]
            }
        }
        else {
            this.setState({endgame: false}, ()=>{ this.setState({endgame: true})})
        }
    }

    // This function is called every time a shot is inserted, and handles all the possible cases (strikes, spares, etc.)
    onScoreInput(evt){
        var input = parseInt(evt.target.value);
        var shots = this.state.progress.shots;
        var firstShot = this.scores[this.state.progress.player][this.state.progress.frame][0];
        if(shots === 1 && this.state.progress.frame < 10 && input + firstShot >10) return; // Invalid Input
        if(shots === 1 && this.state.progress.frame === 10 && firstShot !== 'X' && input + firstShot >10) return; // Invalid Input 

        if(shots === 0 && this.state.progress.frame < 10) { 
            if (input !== 10){ // First shot not a Strike
                this.scores[this.state.progress.player][this.state.progress.frame][0] = input;
                this.setState({progress: {
                    player: this.state.progress.player,
                    shots: this.state.progress.shots + 1,
                    frame: this.state.progress.frame
                }});
            }

            else { //Strike
                this.triggerAnimation()
                this.scores[this.state.progress.player][this.state.progress.frame] = 'X';
                this.setState({progress: {
                    player: this.nextPlayer(this.state.progress.player),
                    shots: 0,
                    frame: this.checkFrame(this.state.progress.frame)
                }});
            }
        }
        else if(shots === 1 && input + firstShot < 10 && this.state.progress.frame < 10) { // Second shot not a Spare
            this.scores[this.state.progress.player][this.state.progress.frame][1] = input
            this.setState({progress: {
                player: this.nextPlayer(this.state.progress.player),
                shots: 0,
                frame: this.checkFrame(this.state.progress.frame)
            }});

        }
        else if (shots === 1 && input + firstShot >= 10 && this.state.progress.frame < 10){ // Spare
            this.scores[this.state.progress.player][this.state.progress.frame][1] = '/'
            this.setState({progress: {
                player: this.nextPlayer(this.state.progress.player),
                shots: 0,
                frame: this.checkFrame(this.state.progress.frame)
            }});

        }

        else if (this.state.progress.frame >= 10){ // Last Frame case
            if(shots === 0) { 
                if (input !== 10){ // First shot not a Strike
                    this.scores[this.state.progress.player][this.state.progress.frame][0] = input;
                    this.setState({progress: {
                        player: this.state.progress.player,
                        shots: this.state.progress.shots + 1,
                        frame: this.state.progress.frame
                    }});
                }
    
                else { // First Shot Strike
                    this.triggerAnimation()
                    this.scores[this.state.progress.player][this.state.progress.frame][0] = 'X';
                    this.setState({progress: {
                        player: this.state.progress.player,
                        shots: 1,
                        frame: this.state.progress.frame
                    }});
                }
            }

            else if(shots === 1 && firstShot !== 10 &&  firstShot + input <10 ) { // No bonus shot case
                if (input === 10) this.triggerAnimation()
                this.scores[this.state.progress.player][this.state.progress.frame][1] = input
                this.scores[this.state.progress.player][this.state.progress.frame+1][0] = input
                this.setState({progress: {
                    player: this.nextPlayer(this.state.progress.player),
                    shots: 0,
                    frame: this.checkFrame(this.state.progress.frame)
                }});
            }
            else if(shots === 1 && (input === 10 || firstShot === 'X' || input + firstShot >= 10)) { // Gets Bonus Shot Case
                if (input === 10) this.triggerAnimation()
                var score = (input === 10) ? 'X' : (input + firstShot >= 10) ? '/' : input
                this.scores[this.state.progress.player][this.state.progress.frame][1] = score
                this.scores[this.state.progress.player][this.state.progress.frame+1][0] = score
                this.setState({progress: {
                    player: this.state.progress.player,
                    shots: 2,
                    frame: this.state.progress.frame
                }});
    
            }
            else if (shots === 2 ){ // Bonus Shot
                if (input === 10) this.triggerAnimation()
                let score = (input === 10) ? 'X' : input
                this.scores[this.state.progress.player][this.state.progress.frame][2] = score
                this.setState({progress: {
                    player: this.nextPlayer(this.state.progress.player),
                    shots: 0,
                    frame: this.checkFrame(this.state.progress.frame)
                }});
    
            }

        }
    }

    // Checks which player goes next
    nextPlayer(p){
        if (this.state.playersNumber === 1) return p;
        if(p === 'player1') return 'player2';
        else return 'player1';
    }

    //  Checks if the 'current frame' has to increse after a player's shot. Also checks if the game is finished
    checkFrame(f){
        if (this.state.playersNumber === 1){
            if (f+1 === 11) this.endgameSave();
            return f+1;
        }
        else {
            if (this.state.progress.player === 'player1') return f;
            else{
                if (f+1 === 11) this.endgameSave();
                return f+1;
            }
        }
    }

    //Saves the data of the match into the browser's localStorage
    endgameSave(){
        var d = new Date();
        var savedata = {
            state: this.state,
            scores: this.scores,
            frameScores: this.frameScores
        }
        savedata.state.endgame = true;
        savedata.state.progress.frame = 11;
        this.setState({endgame: true},() => {localStorage.setItem( d.toLocaleString(), JSON.stringify(savedata))});
    }

    // Returns true if player is a winner
    checkWinner(player){
        if (player === 'player') return true;
        else if (this.frameScores.player1.reduce((a,b) => a+b,0) === this.frameScores.player2.reduce((a,b) => a+b,0)) return true
        else if(player === 'player1') {
            return this.frameScores[player].reduce((a,b) => a+b,0) > this.frameScores.player2.reduce((a,b) => a+b,0)
        }
        else return this.frameScores[player].reduce((a,b) => a+b,0) > this.frameScores.player1.reduce((a,b) => a+b,0)
    }

    // Triggers the Strike animation
    triggerAnimation(){
        this.setState({animation: true})
        setTimeout(() => {this.setState({animation: false})},3000)
    }

    // Reloads the page in order to start a new game
    reloadPage(){
        window.location.reload(false);
    }

    // Calculates the running total for each player
    calculateTotal(player){
        var newscore1 = 0
        var newscore2 = 0
        var nextscore1 = 0
        var nextscore2 = 0
        var nextnextscore1 = 0
        var lastscore = 0
        for(var i = 1; i<=this.state.progress.frame && i<=10;i++){
            newscore1 = (this.scores[player][i][0] === '' || this.scores[player][i][0] === undefined ? 0 : this.scores[player][i][0])
            newscore2 = (this.scores[player][i][1] === '' || this.scores[player][i][1] === undefined  ? 0 : this.scores[player][i][1])
            nextscore1 = (this.scores[player][i+1][0] === '' || this.scores[player][i+1][0] === undefined ? 0 : this.scores[player][i+1][0])
            nextscore2 = (this.scores[player][i+1][1] === '' || this.scores[player][i+1][1] === undefined ? 0 : this.scores[player][i+1][1])
            lastscore = (this.scores[player][10][2] === '' || this.scores[player][10][2] === undefined ? 0 : this.scores[player][10][2])
            if (i>=10){ // Last Frame case
                if (newscore1 === 'X') {
                    if (this.scores[player][10][1] === 'X'){
                        if (this.scores[player][10][2] === 'X'){
                            this.frameScores[player][i] =  30;
                        }
                        else this.frameScores[player][i] =  20 + lastscore;
                    }
                    else this.frameScores[player][i] =  10 + newscore2 + lastscore;
                }
                else if (newscore1 !== 'X' && newscore2 === '/'){
                    if (lastscore === 'X') this.frameScores[player][i] = 20;
                    else this.frameScores[player][i] = 10 + lastscore;
                    
                }
                else this.frameScores[player][i] = newscore1 + newscore2
            }
            else{ // 1-9 Frames
                nextnextscore1 = (this.scores[player][i+2][0] === '' || this.scores[player][i+2][0] === undefined ? 0 : this.scores[player][i+2][0])
                if (newscore1 === 'X') {
                    if (this.state.progress.frame - i >= 2) {
                        if (nextscore1 === 'X'){
                            if (nextnextscore1 === 'X') this.frameScores[player][i] =  30;  // 3 consecutive strikes case
                            else this.frameScores[player][i] =  20 + nextnextscore1;       // 2 consecutive strikes case
                        }
                        else if ( nextscore1 !== 'X' && nextscore2 === '/') this.frameScores[player][i] =  20; // Strike + Spare
                        else this.frameScores[player][i] =  10 + nextscore1 + nextscore2;
                    }
                    else this.frameScores[player][i] =  10; //too early to calculate
                }

                else if (newscore1 !== 'X' && newscore2 === '/'){ //Spare case
                    if (this.state.progress.frame - i >= 1) {
                        if (nextscore1 === 'X') this.frameScores[player][i] =  20;
                        else this.frameScores[player][i] =  10 + nextscore1;
                    }
                    else this.frameScores[player][i] =  10; //too early to calculate
                }
                else this.frameScores[player][i] = newscore1 + newscore2
            }
        }

        return this.frameScores[player].reduce((a,b) => a+b,0);
    }

    render(){
        return (
        <div>
            <Cards pose={this.state.progress.frame>0 ? 'visible' : 'hidden'} className="cards">
                {this.state.players.map(p => {
                    return <div key={p}><ScoreCard pose={(this.state.endgame && this.checkWinner(p)) ? 'winner': 'visible'} key={p} player={p} score={this.scores[p]} total={this.calculateTotal(p)} />{(this.state.endgame && this.checkWinner(p)) ? <div className="winner" key={p + 'w'}>Winner</div>: ''}</div>
                })}
            </Cards>

            <Keyboard pose={!this.state.endgame && this.state.progress.frame<=10 ? 'visible' : 'hidden'} className="keyboard">
                {[0,1,2,3,4,5,6,7,8,9,10].map(k => {
                    return <button key={k} value={k} onClick={this.onScoreInput}>{k}</button>
                })}
            </Keyboard>

            {this.state.endgame ? <button className="refreshButton" onClick={this.reloadPage}>Play Again</button> : ''}

            <PoseGroup>
                {this.state.animation && [
                <Shade key="shade" className="shade" />,
                <Animation key="strike"><img src={Strike} alt="strike" className="animation" key="animation"/></Animation>

                ]}
            </PoseGroup>
                 


        </div>

            
        );
    }

}


export default Game;