import React from 'react';
import './Buttons.css';
import posed from 'react-pose';
import p1img from '../../img/1pm.png';
import p2img from '../../img/2pm.png';

const Box = posed.div({
    hoverable: true,
    init: {
        y:0,
        opacity: 1,
        scale: 1,
        boxShadow: '10px 10px 10px rgba(0, 0, 0, 0)'
    },
    hidden: { y: 400, opacity: 0, transition: { duration: 150 } },
    hover: {
        scale: 1.2,
        boxShadow: '10px 10px 10px rgba(0, 0, 0, 0.25)'
    }
  });
  

const ModeButton = (props) => {


    return (
        <Box className="boxMode" pose={props.pose}>
            <img src={(props.number === '1') ? p1img : p2img} alt='p1' />
            {(props.number === '1') ? '1 Player' : '2 Players'} 
        </Box>
    )

}

const ScoresButton = (props) => {


    return (
        <Box className="boxScores" pose={props.pose}>Previous Scores</Box>
    )

}

export {ModeButton, ScoresButton};