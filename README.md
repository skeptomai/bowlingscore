## Bowling Score Test

![logo](https://gitlab.com/skeptomai/bowlingscore/raw/master/public/logo192.png)

### Installation

In order to install the web application, move into the project's folder and run the following commands:

```
npm install
npm start
```

### Usage

The web application will display a starting menu with three options:
1. 1 player mode.
2. 2 players mode.
3. A button to view previous saved scores.

Selecting 1 or 2 players mode will start the game. A keyboard will appear at the bottom of the screen to insert the score of the single shots. The scores are inserted following the rules of a bowling game: two shots per frame (one in case of a strike). On a second shot, only the valid input's button will work. A total of each player score is displayed at the end of each scorecard. When inserting a strike, the application will trigger an animation (displaying an animated .gif). When the game is complete, the winner player will be highlighted and the score will be saved in case the user wants to view it again in the future (this is achieved by saving the information of the game as a JSON in the local storage of the browser).

### Library Used

The only extra library used is an animation library called [react-pose](https://https://popmotion.io/pose/)